import 'package:flutter/material.dart';
import 'package:location_app/view/ride_booking_page.dart';
import 'package:location_app/view/test.dart';

void main(){

  //server.main();

   runApp(new MaterialApp(
       debugShowCheckedModeBanner: false,
       title: "Navigation App",
      home: new RideBookingPage()
    ));
}