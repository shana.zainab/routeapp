import 'dart:async';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_google_places/flutter_google_places.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_webservice/places.dart';
import 'package:location_app/bloc/navigation_bloc.dart';
import 'package:location_app/model/DirectionInfo.dart';

CameraPosition _initialCameraPosition =
    CameraPosition(target: LatLng(11.9300, 75.5029), zoom: 18.0);

const apiKey = "AIzaSyAJnaXlCDgHAm4j4v2U8CcqAn5ngMsywVE";

class RideBookingPage extends StatefulWidget {
  @override
  _RideBookingPageState createState() => _RideBookingPageState();
}

GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: apiKey);

class _RideBookingPageState extends State<RideBookingPage> {
  NavigationBloc navigationBloc = new NavigationBloc();

  Set<Marker> _markers = Set();

  TextEditingController pickUpLocationController = new TextEditingController();
  TextEditingController dropLocationController = new TextEditingController();

  GoogleMapController controller;
  Geolocator geoLocator;
  GoogleMapsPlaces _places = GoogleMapsPlaces(apiKey: apiKey);

  Color autoCardColor = Colors.white;

DirectionInfo dire = new DirectionInfo.newDirectionInfo();
  bool _pickupSelected = false;
  bool _dropSelected = false;
  bool _showBottom = true;
bool _showRoute = false;
  @override
  initState() {
    super.initState();
    geoLocator = Geolocator();
    getLocationUpdate();
  }

  StreamSubscription stream;

  Future getLocationUpdate() async {
    var locationOptions =
        LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 10);
    stream = geoLocator.getPositionStream(locationOptions).listen((Position position) {
      print(
          "LOCATION UPDATE RECIEVED: ${position.latitude} : ${position.longitude}");
      controller.animateCamera(CameraUpdate.newLatLngZoom(
          new LatLng(position.latitude, position.longitude), 18.0));
    });
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        onWillPop: _onWillPop,
        child: new Scaffold(
          bottomNavigationBar: Visibility(
            visible: _showBottom,
            child: ButtonTheme(
              height: 50,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: new BorderRadius.only(
                        topLeft: const Radius.circular(8.0),
                        topRight: const Radius.circular(8.0))),
                child: Text(
                  "Show Route",
                  style: TextStyle(
                      color: Color.fromRGBO(244, 230, 1, 1),
                      fontWeight: FontWeight.w500,
                      fontSize: 16),
                ),
                color: Color.fromRGBO(41, 46, 66, 1),
                onPressed: () {
                  handleShowRoute();
                },
              ),
            ),
          ),
          body: Stack(
            children: <Widget>[
              StreamBuilder(
                stream: navigationBloc.directionInfo,
                builder: (context, AsyncSnapshot<DirectionInfo> snapshot) {
                  if (snapshot.hasData) {
                    return Container(
                        child: GoogleMap(
                      compassEnabled: false,
                      markers: addMarkers(
                          snapshot.data.destination, snapshot.data.source),
                      polylines:
                          showPolyline(snapshot.data.polylineLatLangList),
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: CameraPosition(
                        target: snapshot.data.source,
                      ),
                      myLocationEnabled: false,
                    ));
                  } else {
                    return Container(
                        child: GoogleMap(
                      markers: _markers,
                      compassEnabled: false,
                      onMapCreated: _onMapCreated,
                      initialCameraPosition: _initialCameraPosition,
                      myLocationEnabled: false,
                    ));
                  }
                },
              ),
              Visibility(
                visible: _showBottom,
                child: locationChooserCard(),
              ),
              topBar(),
              Visibility(
                visible: _showRoute,
                child: Positioned(
                  bottom: 2,
                  child: routeCard(),
                ),
              )
//              StreamBuilder(
//                  stream: navigationBloc.directionInfo,
//                  builder: (context, AsyncSnapshot<DirectionInfo> snapshot) {
//                    if (snapshot.hasData) {
//                      return Positioned(
//                        bottom: 2,
//                        child: routeCard(snapshot.data),
//                      );
//                    } else
//                      return Container();
//                  })
            ],
          ),
        ));
  }

  topBar() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          IconButton(
            icon: Icon(
              Icons.menu,
              size: 30,
              color: Colors.black,
            ),
          ),
          IconButton(
            onPressed: () {
              stream.resume();
              setState(() {
                _showBottom = true;
                _showRoute = false;
              });
            },
            icon: Icon(Icons.directions, size: 30, color: Colors.black),
          )
        ],
      ),
    );
  }

  Future _onMapCreated(GoogleMapController controller) async {
    this.controller = controller;

    geoLocator = Geolocator();
    Position currentPosition = await geoLocator.getCurrentPosition();
    LatLng currentLatLng =
        new LatLng(currentPosition.latitude, currentPosition.longitude);

    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: currentLatLng, zoom: 11.0)));
    setState(() {
      _markers.clear();
      Marker marker = Marker(
        markerId: MarkerId("Your Location"),
        position: currentLatLng,
      );
      _markers.add(marker);
    });
  }

  locationChooserCard() {
    return Card(
        margin: EdgeInsets.only(top: 60, left: 8, right: 8),
        color: Color.fromRGBO(244, 230, 1, 1),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                height: 100,
                width: 20,
                child: Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Icon(Icons.directions_car),
                      Icon(Icons.more_vert),
                      Icon(
                        Icons.location_on,
                        color: Colors.red[900],
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: 3 * MediaQuery.of(context).size.width / 3.8,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Container(
                      decoration: BoxDecoration(
                        color: _pickupSelected
                            ? Color.fromRGBO(41, 46, 66, 1)
                            : Color.fromRGBO(195, 184, 17, 1),
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: FlatButton(
                            onPressed: () {
                              handlePlaceChoose("pickup");
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(2.0))),
                            child: TextFormField(
                              enabled: false,
                              style: TextStyle(
                                color: _pickupSelected
                                    ? Color.fromRGBO(244, 230, 1, 1)
                                    : Color.fromRGBO(41, 46, 66, 1),
                              ),
                              controller: pickUpLocationController,
                              decoration: InputDecoration(
                                hintText: "Pickup Location",
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: _pickupSelected
                                      ? Color.fromRGBO(244, 230, 1, 1)
                                      : Color.fromRGBO(41, 46, 66, 1),
                                ),
                                filled: true,
                                fillColor: _pickupSelected
                                    ? Color.fromRGBO(41, 46, 66, 1)
                                    : Color.fromRGBO(195, 184, 17, 1),
                                contentPadding:
                                    new EdgeInsets.symmetric(vertical: 10.0),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                              ),
                            ),
                          )),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _pickupSelected = false;
                                pickUpLocationController.clear();
                              });
                            },
                            icon: Icon(
                              Icons.close,
                              color: _pickupSelected
                                  ? Color.fromRGBO(244, 230, 1, 1)
                                  : Color.fromRGBO(41, 46, 66, 1),
                              size: 18,
                            ),
                          )
                        ],
                      ),
                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(top:2.0),
//                      child: Row(
//                        crossAxisAlignment: CrossAxisAlignment.end,
//                        mainAxisAlignment: MainAxisAlignment.end,
//                        children: <Widget>[
//                        GestureDetector(child:Text("Set Current Location"),
//                        onTap: (){
//
//                        },)
//                      ],),
//                    ),
                    Padding(
                      padding: EdgeInsets.all(4.0),
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: _dropSelected
                            ? Color.fromRGBO(41, 46, 66, 1)
                            : Color.fromRGBO(195, 184, 17, 1),
                        borderRadius: BorderRadius.all(Radius.circular(4.0)),
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                              child: FlatButton(
                            onPressed: () {
                              handlePlaceChoose("fdf");
                            },
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(2.0))),
                            child: TextFormField(
                              enabled: false,
                              style: TextStyle(
                                color: _dropSelected
                                    ? Color.fromRGBO(244, 230, 1, 1)
                                    : Color.fromRGBO(41, 46, 66, 1),
                              ),
                              controller: dropLocationController,
                              decoration: InputDecoration(
                                hintStyle: TextStyle(
                                  fontWeight: FontWeight.w500,
                                  color: _dropSelected
                                      ? Color.fromRGBO(244, 230, 1, 1)
                                      : Color.fromRGBO(41, 46, 66, 1),
                                ),
                                filled: true,
                                fillColor: _dropSelected
                                    ? Color.fromRGBO(41, 46, 66, 1)
                                    : Color.fromRGBO(195, 184, 17, 1),
                                border: OutlineInputBorder(
                                  borderSide: BorderSide.none,
                                  borderRadius: BorderRadius.circular(10.0),
                                ),
                                contentPadding:
                                    new EdgeInsets.symmetric(vertical: 10.0),
                                hintText: "Drop Location",
                              ),
                            ),
                          )),
                          IconButton(
                            onPressed: () {
                              setState(() {
                                _dropSelected = false;
                                dropLocationController.clear();
                              });
                            },
                            icon: Icon(
                              Icons.close,
                              color: _dropSelected
                                  ? Color.fromRGBO(244, 230, 1, 1)
                                  : Color.fromRGBO(41, 46, 66, 1),
                              size: 18,
                            ),
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  addMarkers(LatLng destination, LatLng source) {
    controller.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: source, zoom: 11.0)));

    Set<Marker> _markers = Set();
    Marker destinationMarker = Marker(
      markerId: MarkerId("destination"),
      position: destination,
    );
    _markers.add(destinationMarker);

    return _markers;
  }

  showPolyline(List<LatLng> polyLatLngList) {
    Set<Polyline> polyLines = new Set();

    final Polyline polyline = Polyline(
      polylineId: PolylineId("Overview"),
      consumeTapEvents: true,
      color: Colors.blue,
      width: 15,
      points: polyLatLngList,
    );
    polyLines.add(polyline);

    return polyLines;
  }

  Future<bool> _onWillPop() {
    //handle onBackPressed
    return showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit the App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => exit(0),
                child: new Text('Yes'),
              ),
            ],
          ),
        ) ??
        false;
  }

  Future<void> handlePlaceChoose(String which) async {
    Prediction p = await PlacesAutocomplete.show(
      context: context,
      apiKey: apiKey,
      mode: Mode.overlay,
    );

    displayPrediction(p, which);
  }

  Future<Null> displayPrediction(Prediction p, String which) async {
    if (p != null) {
      PlacesDetailsResponse detail =
          await _places.getDetailsByPlaceId(p.placeId);
      if (which == "pickup") {
        pickUpLocationController.text = detail.result.formattedAddress;
        setState(() {
          _pickupSelected = true;
        });
      } else {
        dropLocationController.text = detail.result.formattedAddress;
        setState(() {
          _dropSelected = true;
        });
      }
    }
  }

  void handleShowRoute() {


    stream.pause();
    setState(() {
      _showBottom = false;
    });
    navigationBloc
        .getDirection(
            pickUpLocationController.text, dropLocationController.text)
        .then((value) {

      dire = value;
      setState(() {
        _showRoute = true;
      });
//      LatLngBounds bounds =
//          LatLngBounds(southwest: value.source, northeast: value.destination);
//
//      controller.moveCamera(CameraUpdate.newLatLngBounds(bounds, 10));
    });
  }

  routeCard() {
    return Card(
      color: Color.fromRGBO(244, 230, 1, 1),
      margin: EdgeInsets.all(0),
      shape: RoundedRectangleBorder(
        borderRadius: new BorderRadius.only(
            topLeft: const Radius.circular(25.0),
            topRight: const Radius.circular(25.0)),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height / 4,
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, top: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "Route Info",
                    style: TextStyle(color: Colors.blueGrey[900], fontSize: 16),
                  ),
                  GestureDetector(
                      onTap: () {
                        setState(() {
                          _showRoute = false;
                        });
                      },
                      child: Icon(
                        Icons.cancel,
                        color: Colors.blueGrey[900],
                      ))
                ],
              ),
            ),
            Divider(
              endIndent: 16,
              indent: 16,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50.0, right: 50),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 8.0),
                    child: Text("From"),
                  ),
                  Container(
                      width: 200,
                      height: 25,
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(40.0))),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 4.0),
                        child: Text(dire.sourceAddress == null?"":dire.sourceAddress),
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50.0, right: 50, top: 8),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(right: 16.0),
                    child: Text("To"),
                  ),
                  Container(
                      width: 200,
                      height: 25,
                      decoration: new BoxDecoration(
                          color: Colors.white,
                          borderRadius:
                              new BorderRadius.all(Radius.circular(40.0))),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0, top: 4.0),
                        child: Text(dire.destAddress == null?"":dire.destAddress),
                      ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 16.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    dire.routeTravelInfo == null?"":dire.routeTravelInfo,
                    style: TextStyle(color: Colors.blueGrey[900], fontSize: 16),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
