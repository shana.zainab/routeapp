import 'package:geolocator/geolocator.dart';
import 'package:location_app/api/direction_api.dart';
import 'package:location_app/model/DirectionInfo.dart';
import 'package:rxdart/rxdart.dart';

class NavigationBloc {
  final DirectionApi directionApi = DirectionApi();


  final _directionInfo = PublishSubject<DirectionInfo>();

  Observable<DirectionInfo> get directionInfo => _directionInfo.stream;

  Future<DirectionInfo> getDirection(String source, String destination) async {
    DirectionInfo directionInfoResponse =
        await directionApi.getDirection(source, destination);
    _directionInfo.sink.add(directionInfoResponse);

    return directionInfoResponse;
  }

  dispose() {
    _directionInfo.close();
  }

}
