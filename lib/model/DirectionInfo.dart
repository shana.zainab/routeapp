import 'package:google_maps_flutter/google_maps_flutter.dart';

class DirectionInfo{

  LatLng _source;
  LatLng _destination;
  String _routeTravelInfo;
  String _routeEndpointAddress;
  String _sourceAddress;
  String _destAddress;


  DirectionInfo.newDirectionInfo();

  List<LatLng> _polylineLatLangList;

  List<LatLng> get polylineLatLangList => _polylineLatLangList;

  set polylineLatLangList(List<LatLng> value) {
    _polylineLatLangList = value;
  }

  String get sourceAddress => _sourceAddress;

  set sourceAddress(String value) {
    _sourceAddress = value;
  }

  DirectionInfo(this._source, this._destination, this._routeTravelInfo,
      this._routeEndpointAddress,this._polylineLatLangList,this._sourceAddress,this._destAddress);

  String get routeEndpointAddress => _routeEndpointAddress;

  set routeEndpointAddress(String value) {
    _routeEndpointAddress = value;
  }

  String get routeTravelInfo => _routeTravelInfo;

  set routeTravelInfo(String value) {
    _routeTravelInfo = value;
  }

  LatLng get destination => _destination;

  set destination(LatLng value) {
    _destination = value;
  }

  LatLng get source => _source;

  set source(LatLng value) {
    _source = value;
  }

  String get destAddress => _destAddress;

  set destAddress(String value) {
    _destAddress = value;
  }


}