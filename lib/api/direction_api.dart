import 'dart:convert';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location_app/model/DirectionInfo.dart';
import 'package:location_app/model/DirectionResponse.dart' as DR;
import 'package:http/http.dart' as http;

const apiKey = "AIzaSyAJnaXlCDgHAm4j4v2U8CcqAn5ngMsywVE";

class DirectionApi {
  DR.DirectionResponse data;

  getDirection(String source, String destination) async {

    DirectionInfo directionInfo;
    final response = await http.get(
        'https://maps.googleapis.com/maps/api/directions/json?origin=$source&destination=$destination&key=$apiKey');

    data = DR.DirectionResponse.fromJson(json.decode(response.body));

    //process response json
    List<DR.Routes> routes = data.routes;
    for (final route in routes) {
      //show route polyline
      String overviewPolyline = route.overviewPolyline.points;
      List<LatLng> polylineLatLngs = decodePoly(overviewPolyline);

      List<DR.Legs> legs = route.legs;
      for (final leg in legs) {
        //add markers

        directionInfo = new DirectionInfo(
            new LatLng(leg.startLocation.lat, leg.startLocation.lng),
            new LatLng(leg.endLocation.lat, leg.endLocation.lng),
            "${leg.distance.text} (${leg.duration.text})",
            "${leg.startAddress} - ${leg.endAddress}",polylineLatLngs,leg.startAddress,leg.endAddress);
      }
    }
    return directionInfo;
  }

  List<LatLng> decodePoly(String encoded) {
    //convert encoded json polyline points string to list of latlngs

    List<LatLng> poly = new List();
    int index = 0, len = encoded.length;
    int lat = 0, lng = 0;

    while (index < len) {
      int b, shift = 0, result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lat += dlat;

      shift = 0;
      result = 0;
      do {
        b = encoded.codeUnitAt(index++) - 63;
        result |= (b & 0x1f) << shift;
        shift += 5;
      } while (b >= 0x20);
      int dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
      lng += dlng;

      LatLng p = new LatLng(lat / 1E5, lng / 1E5);
      poly.add(p);
    }

    return poly;
  }
}
